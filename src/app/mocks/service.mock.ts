import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class AlertServiceMock {
  success(message: string, keepAfterNavigationChange = false) {
  }
  error(message: string, keepAfterNavigationChange = false) {
  }
  getMessage() {
  }
}
