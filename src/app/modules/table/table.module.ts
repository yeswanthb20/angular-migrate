import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { DatePipe } from '@angular/common';

import { TableRoutingModule } from './table-routing.module';
import { TableComponent } from './table-component/table.component';
import { CustomDateFormatDirective } from '../../directives/custom-date-format.directive';


@NgModule({
  imports: [
    CommonModule,
    MatTableModule,
    TableRoutingModule
  ],
  declarations: [
    TableComponent,
    CustomDateFormatDirective
  ],
  providers: [
    DatePipe
  ],
})
export class TableModule { }
