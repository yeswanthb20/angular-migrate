import { Component, OnInit } from '@angular/core';
import { Table } from '../../../modals/table.modal';
import { SampleService } from '../../../services/sample.service';


@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
  providers: [
    SampleService
  ]
})
export class TableComponent implements OnInit {
  displayedColumns: string[] = ['productId', 'productName', 'price', 'fromDate', 'toDate'];
  jsonData;
  constructor(
    private sampleService: SampleService
  ) { }

  ngOnInit() {
    this.sampleService.getProducts().subscribe((data: Array<Table>) => {
      this.jsonData = data;
    });
  }
}
