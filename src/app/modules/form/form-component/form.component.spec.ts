import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormComponent } from './form.component';
import { RouterTestingModule } from '@angular/router/testing';
import { SampleService } from '../../../services/sample.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ConfigService } from '../../../services/config.service';
import { By } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('FormComponent', () => {
  let component: FormComponent;
  let fixture: ComponentFixture<FormComponent>;
  let sampleService: SampleService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        FormComponent,
      ],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule
      ],
      schemas: [ NO_ERRORS_SCHEMA ],
      providers: [
        ConfigService,
        SampleService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    sampleService = TestBed.get(SampleService);
  });

  it('Formcomponent should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call onsubmit method on save button click', () => {
    spyOn(component, 'onSubmit');
    fixture.debugElement.query(By.css('.btn-primary')).triggerEventHandler('click', null);
    fixture.whenStable().then(() => {
      expect(component.onSubmit).toHaveBeenCalled();
    });
  });

  it('form invalid when empty', () => {
    expect(component.initialDetailsForm.valid).toBeFalsy();
  });

  it('Form component : submiting empty form', () => {
    expect(component.onSubmit()).toEqual(false);
  });

  it('Form component : submiting valid form', () => {
    component.initialDetailsForm.controls['name'].setValue('bhargav');
    component.initialDetailsForm.controls['fromDate'].setValue(new Date(2013, 9, 23));
    component.initialDetailsForm.controls['toDate'].setValue(new Date(2019, 9, 23));

    expect(component.onSubmit()).toEqual(true);
  });
});
