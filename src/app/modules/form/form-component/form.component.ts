import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';

import { SampleService } from '../../../services/sample.service';
import { AlertMessage } from 'src/app/modals/alert-message.modal';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css'],
  providers: [
    SampleService
  ]
})
export class FormComponent implements OnInit {

  disable: boolean;
  message: AlertMessage;

  constructor(
    private sampleService: SampleService) {
  }

  initialDetailsForm: FormGroup;

  ngOnInit(): void {
    this.initialDetailsForm = new FormGroup({
      'name': new FormControl(null, Validators.required),
      'promotions': new FormControl(null),
      'fromDate': new FormControl(null, Validators.required),
      'toDate': new FormControl(null, Validators.required)
    });
  }

  onSubmit() {
    if (this.initialDetailsForm.invalid) {
      return false;
    }
    this.disable = true;
    this.sampleService.saveProducts(this.initialDetailsForm.value).subscribe((data) => {
      this.disable = false;
      this.message = {
        type : 'success',
        text : 'Data Saved Successfully'
      };
    },
    (err) => {
      this.disable = false;
      this.message = {
        type : 'error',
        text : err.message
      };
    });
    return true;
  }

}
