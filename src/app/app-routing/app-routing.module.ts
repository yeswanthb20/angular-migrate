import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthenticateComponent } from '../components/authenticate/authenticate.component';
import { AuthgaurdService } from '../services/authgaurd.service';

const routes: Routes = [
  {path: 'authentiate', component: AuthenticateComponent},
  {path: 'form', loadChildren: '../modules/form/form.module#FormModule', canActivate: [AuthgaurdService]},
  {path: 'table', loadChildren: '../modules/table/table.module#TableModule', canActivate: [AuthgaurdService]},
  {path: '', redirectTo: 'authentiate', pathMatch: 'full'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
