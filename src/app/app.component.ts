import { Component, OnInit } from '@angular/core';

import { AuthenticateService } from './services/authenticate.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  implements OnInit {
  title = 'migrate';
  loggedIn: boolean;

  constructor (
    private authenticateService: AuthenticateService
  ) {

  }

  ngOnInit(): void {
    this.loggedIn = this.authenticateService.isAuthenticated();
    this.authenticateService.authenticationState.subscribe((data) => {
      this.loggedIn = data;
    });
  }
}
