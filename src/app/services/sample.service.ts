import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ConfigService } from './config.service';

@Injectable()
export class SampleService {

  constructor(
    private http: HttpClient,
    private configService: ConfigService
  ) { }

  getProducts() {
    const productsUrl = this.configService.getApiUrl('products');
    return (this.http.get(productsUrl));
  }

  saveProducts(saveData) {
    const productsUrl = this.configService.getApiUrl('products');
    return (this.http.post(productsUrl, saveData));
  }

}
