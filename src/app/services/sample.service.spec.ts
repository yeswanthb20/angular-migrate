import { TestBed } from '@angular/core/testing';

import { SampleService } from './sample.service';
import { HttpClientModule } from '@angular/common/http';
import { ConfigService } from '../services/config.service';

describe('SampleService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule
    ],
    providers: [
      SampleService,
      ConfigService
    ]
  }));

  it('should be created', () => {
    const service: SampleService = TestBed.get(SampleService);
    expect(service).toBeTruthy();
  });
});
