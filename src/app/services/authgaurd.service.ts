import { Injectable } from '@angular/core';

import { Router, CanActivate } from '@angular/router';
import { AuthenticateService } from './authenticate.service';

@Injectable({
  providedIn: 'root'
})
export class AuthgaurdService implements CanActivate {

  constructor(
    private authenticateService: AuthenticateService,
    private router: Router
  ) { }

  canActivate(): boolean {
    if (!this.authenticateService.isAuthenticated()) {
      this.router.navigate(['authentiate']);
      return false;
    }
    return true;
  }
}
