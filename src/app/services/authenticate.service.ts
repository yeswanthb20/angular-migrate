import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { User } from '../modals/user.modal';

@Injectable({
  providedIn: 'root'
})
export class AuthenticateService {
  authenticationState = new BehaviorSubject(false);

  constructor() { }

  register(data) {
    return new Promise((resolve, reject) => {
      if (data && data.username && data.password) {
        const existingUsersString = localStorage.getItem('users');
        let existingUsers: Array<object>;

        if (existingUsersString) {
          existingUsers = JSON.parse(existingUsersString);
        } else {
          existingUsers = [];
        }

        existingUsers.push(data);
        localStorage.setItem('users', JSON.stringify(existingUsers));
        resolve(true);
      } else {
        reject(false);
      }
    });
  }

  login(data) {
    return new Promise((resolve, reject) => {
      if (data && data.username && data.password) {
        const existingUsersString = localStorage.getItem('users');
        let existingUsers: Array<object>;

        if (existingUsersString) {
          existingUsers = JSON.parse(existingUsersString);
        } else {
          existingUsers = [];
        }

        const currentUser = existingUsers.find((user: any) => {
          if (user.username === data.username && user.password === data.password) {
            return user;
          }
        });

        if (currentUser) {
          this.authenticationState.next(true);
          resolve(currentUser);
        } else {
          this.authenticationState.next(false);
          reject(false);
        }
      } else {
        this.authenticationState.next(false);
        reject(false);
      }
    });
  }

  isAuthenticated() {
    return this.authenticationState.value;
  }
}
