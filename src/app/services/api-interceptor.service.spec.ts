import { TestBed } from '@angular/core/testing';

import { ApiInterceptorService } from './api-interceptor.service';

describe('StorageInterceptorService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      ApiInterceptorService
    ]
  }));

  it('should be created', () => {
    const service: ApiInterceptorService = TestBed.get(ApiInterceptorService);
    expect(service).toBeTruthy();
  });
});
