import { TestBed } from '@angular/core/testing';

import { AuthgaurdService } from './authgaurd.service';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';

describe('AuthgaurdService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    schemas: [ NO_ERRORS_SCHEMA ],
    imports: [
      RouterTestingModule
    ]
  }));

  it('should be created', () => {
    const service: AuthgaurdService = TestBed.get(AuthgaurdService);
    expect(service).toBeTruthy();
  });
});
