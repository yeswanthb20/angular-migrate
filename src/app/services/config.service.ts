import { Injectable } from '@angular/core';

import { environment } from '../../environments/environment';
import { apiUrls } from '../config/api-config';

@Injectable()
export class ConfigService {

  constructor() { }

  getApiUrl(key) {
    return environment.baseurl + apiUrls[key];
  }
}
