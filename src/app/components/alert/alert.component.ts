import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {

  message: any;

  constructor() { }

  ngOnInit() {
  }

  @Input('messageObj')
  set allowDay(value: Object) {
    this.message = value;
  }

  closeAlert() {
    this.message = null;
  }
}
