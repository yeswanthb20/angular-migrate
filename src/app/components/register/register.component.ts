import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup, FormGroupDirective } from '@angular/forms';

import { AuthenticateService } from '../../services/authenticate.service';
import { AlertMessage } from 'src/app/modals/alert-message.modal';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  message: AlertMessage;

  constructor(
    private authenticateService: AuthenticateService
  ) { }

  ngOnInit(): void {
    this.registerForm = new FormGroup({
      'name': new FormControl(null, Validators.required),
      'username': new FormControl(null, Validators.required),
      'password': new FormControl(null, Validators.required),
      'confirmPassword': new FormControl(null, Validators.required)
    });
  }

  register(registerFormComp) {
    if (this.registerForm.invalid) {
      return;
    }
    this.authenticateService.register(this.registerForm.value).then((status) => {
      if (status) {
        this.message = {
          type: 'success',
          text: 'Successfully registered. Proceed to login!'
        };
        registerFormComp.reset();
        this.registerForm.reset();
      }
    }, (error) => {
    });
  }
}
