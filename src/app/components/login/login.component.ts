import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthenticateService } from '../../services/authenticate.service';
import { AlertMessage } from 'src/app/modals/alert-message.modal';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  error: String;
  message: AlertMessage;

  constructor(
    private authenticateService: AuthenticateService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      'username': new FormControl(null, Validators.required),
      'password': new FormControl(null, Validators.required)
    });
  }

  login() {
    if (this.loginForm.invalid) {
      return;
    }
    this.authenticateService.login(this.loginForm.value).then((data) => {
      this.router.navigate(['form']);
    }, (error) => {
      this.message = {
        type: 'error',
        text: 'Login failed.'
      };
    });
  }
}
