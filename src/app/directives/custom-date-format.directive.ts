import { Directive, Input, OnInit, ElementRef } from '@angular/core';
import { DatePipe } from '@angular/common';

@Directive({
  selector: '[appCustomDateFormat]'
})
export class CustomDateFormatDirective implements OnInit {

  @Input() dateValue: string;

  constructor(private el: ElementRef, private datepipe: DatePipe) {
  }

  ngOnInit(): void {
    this.el.nativeElement.innerHTML = this.datepipe.transform(this.dateValue, 'MM-dd-yyyy');
  }

}
