export const apiUrls = {
  products: 'products',
  customers: 'saveCustomer',
  updateProfile: 'updateCustomerById',
  deleteProfile: 'deleteCustomerById'
};
