export interface Table {
    productId: number;
    productName: string;
    price: number;
    fromDate: string;
    toDate: string;
}
